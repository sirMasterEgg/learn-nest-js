import { Injectable } from '@nestjs/common';
import { IUser } from './interface/user.interface';

@Injectable()
export class UserService {
  private readonly users: IUser[] = [];

  create(user: IUser): IUser {
    this.users.push(user);
    return user;
  }

  findAll(): IUser[] {
    return this.users;
  }

  findOne(username: string): IUser {
    return this.users.find((user) => user.username === username);
  }
}
