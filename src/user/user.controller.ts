import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { IUser } from './interface/user.interface';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}
  @Get()
  getUsers(): IUser[] {
    return this.userService.findAll();
  }

  @Get(':username')
  getSingleUser(@Param('username') username: string): IUser {
    return this.userService.findOne(username);
  }

  @Post()
  createUser(@Body() user: IUser): IUser {
    return this.userService.create(user);
  }
}
